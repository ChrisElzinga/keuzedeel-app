package com.example.keuzedeel_app;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;



public class MainActivity extends AppCompatActivity {

    private Button btnToggleDark;
    private SharedPreferences.Editor editor;
    public static boolean isDarkModeOn = false;
    private MainActivity intent;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        intent = this;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnToggleDark = findViewById(R.id.btnToggleDark);

        // Saving state of our app
        // using SharedPreferences
        SharedPreferences sharedPreferences = getSharedPreferences("sharedPrefs", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        isDarkModeOn = sharedPreferences.getBoolean("isDarkModeOn", false);

        setDarkMode(isDarkModeOn, false); //Load current mode

        btnToggleDark.setOnClickListener(
                new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        setDarkMode(isDarkModeOn, true);
                    }
                });


        Button button1 = findViewById(R.id.button1);
        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.out.println("Button Clicked");

                Intent activity2Intent = new Intent(getApplicationContext(), Activity2.class);
                startActivity(activity2Intent);
            }
        });

        Button button2 = findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.out.println("Button Clicked");

                Intent activity3Intent = new Intent(getApplicationContext(), Activity3.class);
                startActivity(activity3Intent);
            }

        });
    }

    private void setDarkMode(Boolean darkmode, boolean clickEvent) {
        String btnText = (darkmode) ? "Enable Dark Mode" : "Disable Dark Mode"; //Calculate the text for the toggle dark mode button
        int acd = (darkmode) ? AppCompatDelegate.MODE_NIGHT_NO : AppCompatDelegate.MODE_NIGHT_YES; //Calculate the AppCompatDelegate for the intent

        //Set the correct activity when changing nightmode
        AppCompatDelegate.setDefaultNightMode(acd);

        //Check if the function has been called by the clickevent
        if (clickEvent) {
            editor.putBoolean("isDarkModeOn", !darkmode); //Set isDarkModeOn Boolean to the new state of the darkmode
            editor.apply(); //Apply changes that are updated by the isDarkModeOn Boolean
    }

        //Update text of the toggle darkmode button
        btnToggleDark.setText(btnText);

        //Check if the function has been called by the clickevent
        if (clickEvent) {
            intent.recreate(); //Recreate the current intent to show the new dark/light mode
        }
    }
}
