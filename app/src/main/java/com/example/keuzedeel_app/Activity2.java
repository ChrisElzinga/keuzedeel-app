package com.example.keuzedeel_app;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;


import androidx.appcompat.app.AppCompatActivity;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import java.util.Set;

public class Activity2 extends AppCompatActivity {

    private static final String TAG =  "Activity2";

    LineChart mpLineChart;
    private TextView mDisplayDate;
    private DatePickerDialog.OnDateSetListener mDateSetListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);

        mDisplayDate = (Button) findViewById(R.id.editTextButton);

        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();


                String currentDate = (String) mDisplayDate.getText();
                String[] date = currentDate.split("/");

                Boolean getCurrentDate = currentDate.equalsIgnoreCase("SELECTEER DATUM");
                int year = (getCurrentDate) ? cal.get(Calendar.YEAR) : Integer.parseInt(date[2]);
                int month = (getCurrentDate) ? cal.get(Calendar.MONTH) : Integer.parseInt(date[1]) -1;
                int day = (getCurrentDate) ? cal.get(Calendar.DAY_OF_MONTH) : Integer.parseInt(date[0]);

                @SuppressLint("ResourceAsColor") DatePickerDialog dialog = new DatePickerDialog(Activity2.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year,month,day);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.R.color.transparent));
                        dialog.show();


            }
        });

        refreshChart();
        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                Log.d(TAG,"onDateSet: MM/DD/YYY: " + month + "/" + day + "/" + year);

                String date = day + "/" + (month + 1) + "/" + year;
                mDisplayDate.setText(date);
            }
        };

        Button button3 = findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                System.out.println("Button Clicked");
                

//
                Intent activity3Intent = new Intent(getApplicationContext(), Activity3.class);
                startActivity(activity3Intent);
            }

        });

        System.out.println("ACTIVITY BUKEEEEEEEE");
    }

    private void refreshChart() {
        mpLineChart = (LineChart) findViewById(R.id.activity_main_linechart);
        LineDataSet LineDataSet1 = new LineDataSet(dataValues(), "Gewicht");
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(LineDataSet1);

        LineData data = new LineData(dataSets);
        mpLineChart.setData(data);
        mpLineChart.invalidate();
    }

    private void addDataValue(int x, int y) {
        Gson gson = new Gson();
        SharedPreferences sharedPreferences = getSharedPreferences("sharedPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        ArrayList<Entry> data = gson.fromJson(sharedPreferences.getString("dataValues", null), new TypeToken<ArrayList<Entry>>(){}.getType());
        if (data == null) {
            data = new ArrayList<Entry>();
        }

        System.out.println(data);
        System.out.println("GURBE MYN JONG ========================================================================================================================================================================================================================================================");
        data.add(new Entry(x, y));

        String json = gson.toJson(data);
        editor.putString("dataValues", json);
        editor.commit();
    }

    private ArrayList<Entry> dataValues() {
        Gson gson = new Gson();
        SharedPreferences sharedPreferences = getSharedPreferences("sharedPrefs", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(20);
        list.add(24);
        list.add(2);
        list.add(10);
        list.add(28);
        list.add(15);

        EditText weightEditText;
        weightEditText = (EditText) findViewById(R.id.editTextNumberDecimal);

        Button submit;
        submit = (Button) findViewById(R.id.button);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String weight= weightEditText.getText().toString();
                int finalValue=Integer.parseInt(weight);
                list.add(finalValue);
                System.out.println("dikzak");
            }
        });

        ArrayList<Entry> dataVals = new ArrayList<Entry>();

        for (int i = 0; i < list.size(); i++) {
            Integer value = list.get(i);
            dataVals.add(new Entry(i,value));
        }

        return dataVals;

        /*String currentValues = sharedPreferences.getString("dataValues" ,null);
        if (currentValues != null) {
            ArrayList<Entry> data = gson.fromJson(sharedPreferences.getString("dataValues", null), new TypeToken<ArrayList<Entry>>(){}.getType());
            return data;
        } else {
            return new ArrayList<Entry>();
        }*/
    }


}